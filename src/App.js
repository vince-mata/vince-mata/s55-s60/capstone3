import { useState, useEffect } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';






import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Cart from "./pages/Cart";
import Admin from "./pages/Admin";




import './App.css';
import { UserProvider } from './UserContext';


function App() {

    const [user, setUser] = useState({
      //email: localStorage.getItem('email')
      id: null,
      isAdmin: null
    })
    
    //Function toclear the localstorage for logout
    const unsetUser = () => {
      localStorage.clear();
    }

    
    useEffect( () => {
      if(localStorage.getItem("token") !== null){
      fetch("https://shrouded-temple-77293.herokuapp.com/users/details", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
       }
      })
      .then(res => res.json())
      .then(data => {
          setUser({
              id: data._id,
              isAdmin: data.isAdmin,
              email: data.email
          });
      });
    }
  }, []);


    return (
    <UserProvider value= {{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar />
            <Routes>
                <Route exact path="/" element={<Home/>} />
                <Route exact path="/login" element={<Login/>} />
                <Route exact path="/register" element={<Register/>} />
                <Route exact path="/logout" element={<Logout/>} />
                <Route exact path="/products" element={<Products/>} />
                <Route exact path="/cart" element={<Cart/>} />
                <Route exact path="/products/:productId" element={<ProductView/>} />
                <Route exact path="/admin" element={<Admin/>} />

                <Route path="*" element={<Error />} />
               
            </Routes>
        </Router>
    </UserProvider>
  );
}

export default App;
