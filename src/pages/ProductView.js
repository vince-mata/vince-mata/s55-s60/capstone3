import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import Image from 'react-bootstrap/Image'

export default function ProductView() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate()

    let [quantity, setQuantity] = useState(1);

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState();

    const increment = () => setQuantity(quantity++);
    const decrement = () => {
        if(quantity > 1){
            setQuantity(quantity--);
        }
        else{
            setQuantity(1);
        }
    };

    function addToCart(){
        fetch("https://shrouded-temple-77293.herokuapp.com/users/addtocart", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            Swal.fire({
                title: "Item Added To Cart!",
                icon: "success",
                text: "The item is added to your cart."
            });
        });
    }

    function buyNow(){
        fetch("https://shrouded-temple-77293.herokuapp.com/orders/buynow", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity,
            })
        })
        .then(res => res.json())
        .then(data => {
            Swal.fire({
                title: "Order Successful!",
                icon: "success",
                text: "You can now see your order."
            });
            navigate('/products')

        });
    }

    useEffect(() => {
        fetch(`https://shrouded-temple-77293.herokuapp.com/products/${productId}`, )
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setIsActive(data.isActive);
        });
    }, [productId]);

    return (
        (!user.isAdmin)
        ?
        <Container className="mt-5">
            <Row>
                <Col md={6}>
                    <img src="https://i.pinimg.com/564x/36/17/f2/3617f26414b96db9e598f913af10d828.jpg"/>
                </Col>
                <Col md={6}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>
                                <h1>{name}</h1>
                            </Card.Title>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Quantity:</Card.Subtitle>
                            <Row className="mt-2 mb-2">
                                <Col>
                                {
                                    (quantity > 1)
                                    ?
                                    <Button variant="dark" className="btn-block" onClick={decrement}>-</Button>
                                    :
                                    <Button variant="dark" className="btn-block" disabled={true}>-</Button>
                                }
                                </Col>
                                <Col>
                                    <Form.Control 
                                        type="number"
                                        value={quantity}
                                        onChange={e => setQuantity(e.target.value)}
                                        disabled
                                        />
                                </Col>
                                <Col>
                                    <Button variant="dark" className="btn-block" onClick={increment}>+</Button>
                                </Col>
                            </Row>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>&#8369; {price * quantity}</Card.Text>
                            {
                                (isActive)
                                ?
                                    ((user.id !== null) || (localStorage.getItem("token") !== null))
                                    ?
                                    <Fragment>
                                        <Row>
                                            <Col>
                                                <Button variant="dark" onClick={() => addToCart(productId)}>Add to Cart</Button>
                                            </Col>
                                            <Col>
                                                <Button variant="dark" onClick={() => buyNow(productId)}>Buy Now</Button>
                                            </Col>
                                        </Row>
                                    </Fragment>
                                    :
                                    <Button variant="danger" as={Link} to="/login">Log In to Order</Button>
                                :
                                    <Fragment>
                                        <p>This item is out of stock.</p>
                                    </Fragment>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        :
        <Navigate to="/products"/>
    )
}



