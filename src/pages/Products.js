import { Fragment, useEffect, useState, useContext } from 'react';
import { Container } from "react-bootstrap";
import ProductsCards from "../components/ProductCard";
import UserContext from "../UserContext";

const Shop = () => {
    const {user} = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        if(!user.isAdmin){
            fetch("https://shrouded-temple-77293.herokuapp.com/products/", )
            .then(res => res.json())
            .then(data => {
                setProducts(data.map(product => {
                    return (
                        <ProductsCards
                            key={product._id}
                            productProp={product}
                        />
                    )
                }));
            });
        }
    }, []);

    return (
        
        <Container>
            <h1 className="text-center mt-3 mb-3">Products</h1>
            <Fragment>
                {products}
            </Fragment>
        </Container>
    )
}

export default Shop;
