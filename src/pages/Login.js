import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from "../UserContext";



export default function Login() {

    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate()
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

        function loginUser(e) {
        e.preventDefault();

        fetch("https://shrouded-temple-77293.herokuapp.com/users/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.accessToken !== "undefined") {

                    localStorage.setItem('token', data.access)
                    retrieveUserDetails(data.access)
                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "You can now shop online."
                });
                navigate('/products')
            }
            else{
                Swal.fire({
                    title: "Wrong Email Or Password!",
                    icon: "error",
                    text: "Please enter your correct email or password.",
                })
            }
        });
    }

    const retrieveUserDetails = (token) => {
        fetch("https://shrouded-temple-77293.herokuapp.com/users/details", {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id:data._id,
                isAdmin:data.isAdmin,
                email: email
            });
        });
    }

    useEffect(() => {
        if((email !== "" && password !== "")){
            setIsActive(true);
        }
        else{
            setIsActive(false);         
        }
    }, [email, password]);


return (
    ((user.id !== null) || (localStorage.getItem("token") !== null))
    ?
        (user.isAdmin)
        ?
        <Navigate to="/admin"/>
        :
        <Navigate to="/products" />
        :

    <Container>
        <div className="d-flex justify-content-center mt-5">
            <Card className="text-center center-block cardForm">
                <Card.Header className="cardHeader">
                    <h3>Log In</h3>
                    </Card.Header>
                        <Card.Body>
                            <Form onSubmit={(e) => loginUser(e)}>          
                            <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control 
                            type="email" 
                            placeholder="user@email.com" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Enter your password" 
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required>
                        </Form.Control>
                    </Form.Group>
                    {
                        isActive 
                         ? 
                        <Button variant="dark" type="submit" className="submitBtn">Log In</Button>
                        :
                        <Button variant="dark" type="submit" className="submitBtn" disabled>Log In</Button>
                            }
                        </Form>
                    </Card.Body>
                    <Card.Body>
                        Don't have an account? <Link to="/register">Register Here</Link>
                    </Card.Body>
                </Card>
            </div>
        </Container>
    )
}


