import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register (){

  const { user } = useContext(UserContext)
  const navigate = useNavigate()
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);


  function registerUser(e) {

    e.preventDefault()

          fetch("https://shrouded-temple-77293.herokuapp.com/users/signup", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email
                })
              })
            .then(res => res.json())
            .then(data => {
                if(data) { //meaning email is not unique
                  Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please try another email address"
                  })

              } else {
                
                fetch("https://shrouded-temple-77293.herokuapp.com/users/login", {
                method: 'POST',
                headers: {
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({
                    email: email,
                    password: password1
              })
           })
            .then(res => res.json())
            .then(data => {
                if(data) { //meaning email is not unique
                  Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "You may now Log in"
                  })
                  navigate('/login')

              } else {

                  Swal.fire({
                    title: "Registration failed",
                    icon: "error",
                    text: "Please try again"
                 })   
              }
            })
          }
        })  
      }         
  useEffect(() => {

    if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
      setIsActive(true);
    } else {
      setIsActive(false)
    }

  }, [email, password1, password2])


  return(

      <Container>

            <div className="d-flex justify-content-center mt-5">
                <Card className="text-center center-block cardForm">
                    <Card.Header className="cardHeader">
                        <h3>Register</h3>
                    </Card.Header>
                    <Card.Body>
                        <Form onSubmit={(e) => registerUser(e)}>
                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="user@email.com" 
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Enter your password" 
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)}
                                    required>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="password2">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Please verify password" 
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required></Form.Control>
                            </Form.Group>
                            {
                                isActive ?
                                <Button variant="dark" type="submit" className="submitBtn">Register</Button>
                                :
                                <Button variant="dark" type="submit" className="submitBtn" disabled>Register</Button>
                            }
                        </Form>
                    </Card.Body>
                    <Card.Body>
                        Already have an account? <Link to="/login">Log In Here</Link>
                    </Card.Body>
                </Card>
            </div>
        </Container>
    )
}

