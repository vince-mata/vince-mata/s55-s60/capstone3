import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import AppNavbar from './AppNavbar';

//Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);