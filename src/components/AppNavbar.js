import { Fragment, useState, useContext } from "react";
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from "react-router-dom";
import UserContext from '../UserContext';


export default function AppNavbar(){

  const { user } = useContext(UserContext);
    return (
        <Navbar expand="lg" className="appNavbar" variant="light">
            <Container>
                <Navbar.Brand as={Link} to="/">Gaming Essentials</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    {
                        (user.id !== null && user.isAdmin)
                        ?
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to="/admin" exact>Dashboard</Nav.Link>
                            
                        </Nav>
                        :
                        <Nav className="me-auto">
                            
                            <Nav.Link as={Link} to="/products" exact>Products</Nav.Link>
                            {
                                (user.id !== null && !user.isAdmin)
                                ?
                                <>
                                    <Nav.Link as={Link} to="/cart" exact>Cart</Nav.Link>
                                    
                                </>
                                :
                                <></>
                            }
                        </Nav>
                    }
                    {
                        ((user.id !== null) || (localStorage.getItem("token") !== null))
                        ?
                            <NavDropdown title={<span className="navDropdownText">{user.email}</span>} id="basic-nav-dropdown" className="navDropdown">
                                
                                <NavDropdown.Item as={NavLink} to="/logout">Log Out</NavDropdown.Item>
                            </NavDropdown>
                        :
                            <Nav>
                                <Nav.Link as={Link} to="/login" exact>Log In</Nav.Link>
                                <Nav.Link as={Link} to="/register" exact>Register
                            </Nav.Link>
                            </Nav>
                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}


