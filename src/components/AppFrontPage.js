import { useState, useEffect } from "react";
import { Link } from "react-router-dom"; 
import { Carousel, Button, ListGroupItem } from "react-bootstrap";

const AppCarouselPage = () => {

  const carouselItems = [
        {
            id: 1,
            img: {
                src: "img/carousel-1.jpg",
                alt: "First Slide"
            },
            captions: {
                title: "",
                text: ""
            }
        },
        {
            id: 2,
            img: {
                src: "img/carousel-2.jpg",
                alt: "Second Slide"
            },
            captions: {
                title: "",
                text: ""
            }
        },
        {
            id: 3,
            img: {
                src: "img/carousel-3.jpg",
                alt: "Third Slide"
            },
            captions: {
                title: "",
                text: ""
            }
        }
    ];

    return (
        <Carousel fade>
            {
                carouselItems.map(item => {
                    const {id, img, captions} = item;
                    return(
                        <Carousel.Item className="frontPageItem" key={id}>
                            <img 
                                className="d-block w-100"
                                src={img.src} 
                                alt={img.alt}
                            />
                            <Carousel.Caption>
                                <h1 className="mb-5">{captions.title}</h1>
                                <h4 className="mb-5">{captions.text}</h4>

                                <Button as={Link} to="/products" variant="dark">Shop Now</Button>{' '}
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    )
}
export default AppCarouselPage;

