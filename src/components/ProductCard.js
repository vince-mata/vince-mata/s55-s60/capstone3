
import { Card, Row, Col } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const ProductCard = ({ productProp }) => {
    const {_id, name, description, price} = productProp;
    return (
        <Card className="mb-3 text-center">
            <Card.Body>
                <Container fluid>
                    <Row>
                        <Col lg={{ span:6, offset: 3}}>
                            <img src="img/carousel-1.jpg"/>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>&#8369; {price}</Card.Text>
                            <Link className="btn btn-dark productBtn" to={`/products/${_id}`}>Details</Link>
                        </Col>
                    </Row>
                </Container>   
            </Card.Body>
         </Card>
    )
}

export default ProductCard;
